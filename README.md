# ReverseOnlinewahnPuzzle

This small GUI application is thought to solve a puzzle from the website [onlinewahn.de](https://www.onlinewahn.de/).

## How does it work?

This program utilises the fact that the generator stores all images on the server and has a known format of naming the
tiles. Thus this program downloads all tiles and then glues them together via the pre known location.

Afterwards it just saves the combined image to the specified location.

## How do you use it?

1. Open the program.
2. Enter the URL in the following manner: `https://www.onlinewahn.de/generator/puzzle/<ID>/` (the trailing slash is
   optional)
3. Choose a folder of your liking.
4. Enter a valid filename. A filetype can be skipped as it will be set by the program.
5. Set the Width and Height. Take the numbers you can see from the puzzle piece in the right bottom corner. If you don't
   find it just take the width and height and subtract one from each side. 

## Limitations

This program will try to respect the serverowner of given website and limits the maximum download jobs at once to not
ddos it. If the pre known tilenames are changing this program will not try to adapt but instead will be archived.

Use this program at your own risk as stated in the `LICENSE`. 
