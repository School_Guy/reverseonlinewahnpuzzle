package de.gotthold.main;

import javafx.application.Platform;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class InetDownloads {

    public byte[] download(URL url) {
        byte[] response = null;
        try {
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            response = out.toByteArray();
        } catch (IOException e) {
            Platform.runLater(() -> Alerts.ExceptionDialog(e,"IOException","Fehler bei der Verbindung zu: " + url.toString()));
        }
        return response;
    }


}
