package de.gotthold.main;

import javafx.application.Platform;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class DownloadPictures {
    private byte[][][] pictures;

    public DownloadPictures(String baseUrl, int width, int height) {
        width++;
        height++;
        List<Runnable> list = new ArrayList<>(width * height);
        pictures = new byte[width][height][];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                list.add(createRunnable(baseUrl, i, j));
            }
        }

        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (Runnable runnable : list) {
            executor.execute(runnable);
        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            System.err.println("Error");
        }
    }

    private Runnable createRunnable(String baseUrl, int xInt, int yInt) {
        String x = String.format("%02d", xInt);
        String y = String.format("%02d", yInt);
        return () -> {
            InetDownloads inetDownloads = new InetDownloads();
            try {
                if (baseUrl.substring(baseUrl.length() - 2).equals("/")) {
                    pictures[xInt][yInt] = inetDownloads.download(new URL(baseUrl + "t-" + x + "-" + y + ".gif"));
                } else {
                    pictures[xInt][yInt] = inetDownloads.download(new URL(baseUrl + "/t-" + x + "-" + y + ".gif"));
                }
            } catch (MalformedURLException e) {
                Platform.runLater(() -> Alerts.ExceptionDialog(e, "MalformedURLException", "Fehler beim Erstellen der URL."));
            }
        };
    }

    public byte[][][] getPictures() {
        return pictures;
    }
}
