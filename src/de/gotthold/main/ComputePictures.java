package de.gotthold.main;

import javafx.application.Platform;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ComputePictures {

    private BufferedImage[][] images;

    public ComputePictures(byte[][][] pictures) {
        images = new BufferedImage[pictures.length][pictures[0].length];
        for (int i = 0; i < pictures.length; i++) {
            for (int j = 0; j < pictures[0].length; j++) {
                BufferedImage temp = createImageFromBytes(pictures[i][j]);
                if (temp != null) {
                    images[i][j] = temp;
                }
            }
        }
    }

    //From: https://stackoverflow.com/questions/2318020/merging-two-images
    public BufferedImage generateFinalResult() {
        int width = images[0][0].getWidth() * images.length - images.length * 31 + 32;
        int height = images[0][0].getHeight() * images[0].length - images[0].length * 31 + 32;
        BufferedImage result = new BufferedImage(width, height, ColorSpace.TYPE_RGB);
        Graphics g = result.getGraphics();
        for (int i = 0; i < images.length; i++) {
            for (int j = 0; j < images[0].length; j++) {
                if (i == 0 & j == 0) {
                    int x = 0;
                    int y = 0;
                    g.drawImage(images[i][j], x, y, null);
                } else if (i == 0) {
                    int x = 0;
                    int y = j * images[0][0].getHeight() - j * 31;
                    g.drawImage(images[i][j], x, y, null);
                } else if (j == 0) {
                    int x = i * images[0][0].getWidth() - i * 31;
                    int y = 0;
                    g.drawImage(images[i][j], x, y, null);
                } else {
                    int x = i * images[0][0].getWidth() - i * 31;
                    int y = j * images[0][0].getHeight() - j * 31;
                    g.drawImage(images[i][j], x, y, null);
                }
            }
        }
        return result;
    }

    //From: https://stackoverflow.com/questions/12705385/how-to-convert-a-byte-to-a-bufferedimage-in-java
    private BufferedImage createImageFromBytes(byte[] imageData) {
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        try {
            return ImageIO.read(bais);
        } catch (IOException e) {
            Platform.runLater(() -> Alerts.ExceptionDialog(e, "IOException", "Fehler beim generieren eines Puzzlestuecks"));
            return null;
        }
    }
}
