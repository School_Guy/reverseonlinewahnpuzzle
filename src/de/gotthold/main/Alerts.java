package de.gotthold.main;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Alerts {

    public static void Dialog(Alert.AlertType type, Window window, String title, String headerText, String contentText) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.initStyle(StageStyle.UTILITY);
        alert.initOwner(window);
        alert.getDialogPane().requestFocus();

        alert.showAndWait();
    }

    public static void Dialog(Alert.AlertType type, Window window, String title, String contentText) {
        Dialog(type, window, title, null, contentText);
    }

    public static void Dialog(Alert.AlertType type, String title, String headerText, String contentText) {
        Dialog(type, null, title, headerText, contentText);
    }

    public static void Dialog(Alert.AlertType type, String title, String contentText) {
        Dialog(type, null, title, null, contentText);
    }

    public static void ExceptionDialog(Exception ex, String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.initStyle(StageStyle.UTILITY);

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public static void ExceptionDialog(Exception ex, String title, String content) {
        ExceptionDialog(ex, title, null, content);
    }

}
