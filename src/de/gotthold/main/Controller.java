package de.gotthold.main;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

public class Controller implements PropertyChangeListener {

    @FXML
    public TextField textFieldX;
    @FXML
    public TextField textFieldY;
    @FXML
    public BorderPane borderPaneMain;
    @FXML
    public Label labelStatus;
    @FXML
    public ProgressBar progressBarStatus;
    @FXML
    public StackPane stackPaneRoot;
    @FXML
    private GridPane gridPaneMain;
    @FXML
    private TextField textFieldUrl;
    @FXML
    private Button buttonSolve;
    @FXML
    private TextField textFieldFileName;
    @FXML
    private Button buttonFolderChooser;
    @FXML
    private TextField textFieldOutputFolder;

    @FXML
    public void initialize() {
        buttonFolderChooser.setOnAction((event -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle("Ausgabeort auswählen");
            Node source = (Node) event.getSource();
            Window stage = source.getScene().getWindow();
            File file = directoryChooser.showDialog(stage);
            if (file != null) {
                textFieldOutputFolder.setText(file.getAbsolutePath());
            }
        }));

        buttonSolve.setOnAction((event -> {
            gridPaneMain.setDisable(true);
            borderPaneMain.setVisible(true);
            SwingWorker swingWorker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    //URL da? & Ordner da? & Dateiname da?
                    if (!textFieldUrl.getText().equals("") & !textFieldOutputFolder.getText().equals("") & !textFieldFileName.getText().equals("")) {
                        borderPaneMain.setOpacity(1);
                        borderPaneMain.toFront();
                        gridPaneMain.toBack();

                        //Dateien downloaden
                        setProgress(1);
                        DownloadPictures downloadPictures = new DownloadPictures(textFieldUrl.getText(), Integer.valueOf(textFieldX.getText()), Integer.valueOf(textFieldY.getText()));
                        byte[][][] bytePictures = downloadPictures.getPictures();
                        Thread.sleep(1000);

                        //Bilder zusammensetzen
                        setProgress(getProgress() + 33);
                        ComputePictures computePictures = new ComputePictures(bytePictures);
                        Thread.sleep(1000);

                        setProgress(getProgress() + 33);
                        BufferedImage result = computePictures.generateFinalResult();
                        Thread.sleep(1000);

                        //Datei speichern
                        setProgress(getProgress() + 33);
                        try {
                            ImageIO.write(result, "png", new File(textFieldOutputFolder.getText(), textFieldFileName.getText() + ".png"));
                        } catch (IOException e) {
                            Alerts.ExceptionDialog(e, "IOException", "Fehler beim speichern der Datei");
                        }
                        Thread.sleep(1000);
                    } else {
                        Alerts.Dialog(Alert.AlertType.ERROR, ((Node) event.getTarget()).getScene().getWindow(), "Leeres Feld", "Eines der Felder war Leer! Es wird nichts gemacht!");
                    }
                    return null;
                }

                @Override
                public void done() {
                    borderPaneMain.setOpacity(0);
                    gridPaneMain.setDisable(false);
                    borderPaneMain.setVisible(false);
                }
            };

            swingWorker.addPropertyChangeListener(this);
            swingWorker.execute();
        }));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("progress".equals(evt.getPropertyName())) {
            if ((Integer) evt.getNewValue() == 1) {
                Platform.runLater(() -> labelStatus.setText("Bilder downloaden..."));
            } else if ((Integer) evt.getNewValue() == 33) {
                Platform.runLater(() -> labelStatus.setText("Bilder zusammensetzen..."));
            } else if ((Integer) evt.getNewValue() == 66) {
                Platform.runLater(() -> labelStatus.setText("Bild speichern..."));
            }
            Platform.runLater(() -> progressBarStatus.setProgress((Integer) evt.getNewValue() / 100));
        }
    }
}
